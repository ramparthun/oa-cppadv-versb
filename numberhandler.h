#ifndef NUMBERHANDLER_H
#define NUMBERHANDLER_H
#include <functional>
template <typename T,typename CONTAINER>
class NumberHandler
{
    CONTAINER container;
public:
    void fill(int count,std::function<T()> generator){
        for(int i=0;i<count;i++){
            container.push_back(generator());
        }
    }
    CONTAINER select(std::function<bool(T)> filter){
        CONTAINER selected;
        for(auto it:container){
            if(filter(it)){
                selected.push_back(it);
            }
        }
        return selected;
    }
    const CONTAINER& list()const{
        return container;
    }
};

#endif // NUMBERHANDLER_H
