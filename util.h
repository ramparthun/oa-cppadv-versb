#ifndef UTIL_H
#define UTIL_H
#include <string>
#include <QRandomGenerator>
class Util
{
public:
    template<typename T>
    static T random(T min,T max){
        QRandomGenerator generator;
        return generator.bounded(min,max);
    }
    template<typename T>
    static bool isPrime(T number){
        bool assist=false;
        for(T i=1;i<number;i++){
            if((number%i)==0){
                assist=true;
            }
        }
        return assist;
    }
    static std::string formatList(auto container){
        std::string collector;
        for(int i=0;i<container.size();i++){
            collector+=container[i];
        }
        return collector;
    }
};

#endif // UTIL_H
